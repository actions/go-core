package cmdcore

import "errors"
import "os/exec"
import "strings"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// ErrNoCommand - Error returned when no command was specified
var ErrNoCommand = errors.New("No command was specified")

// Run - runs command. It does not support streams yet!
func Run(ctx action.Map) (action.Map, error) {
	var bin string    // command name
	var args []string // command arguments

	// Command name with arguments
	name := ctx.Pop("name")

	// Check if name is not empty
	if name.IsNil() {
		return ctx, ErrNoCommand
	}

	// Split cmd to a name and args
	var words []string
	if cmdname, ok := name.String(); ok {
		words = strings.Split(cmdname, " ")
	} else {
		words, _ = name.StringList()
	}

	if len(words) > 0 {
		bin = words[0]
		args = words[1:]
	}

	// Get command arguments
	a := ctx.Pop("args")
	if arg, ok := a.String(); ok {
		args = append(args, strings.Split(arg, " ")...)
	} else if arg, ok := a.StringList(); ok {
		args = append(args, arg...)
	}

	// Execute command
	cmd := exec.Command(bin, args...)

	// Run command and get output
	output, err := cmd.CombinedOutput()

	// Action result
	result := action.Map{
		"success": cmd.ProcessState.Success(),
		"output":  output,
		"time": action.Map{
			"system": cmd.ProcessState.SystemTime(),
			"user":   cmd.ProcessState.UserTime(),
		},
	}

	if err != nil {
		result["error"] = err.Error()
		return result, err
	}

	return result, nil
}

func init() {
	core.Add("cmd.run", core.Function{
		Description: "Runs command",
		Arguments: core.Variables{
			"name": core.String("Command name", ""),
			"args": core.Interface("Command arguments (string or list)", nil),
		},
		Returns: core.Variables{
			"success": core.Bool("Execution state", false),
			"output":  core.Interface("Command output", nil),
			"time":    core.Map("Execution time (user & system)", nil),
		},
		Function: Run,
	})
}
