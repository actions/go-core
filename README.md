# Core

Packages contained in this repository are exposing core functions for go-actions.

## Testing

Core tests are in `tests` directory

```sh
# Test using github.com/crackcomm/cmds
$ cmds test
# equivalent to:
$ go install github.com/crackcomm/action-test
$ action-test -tests=./tests/* -sources=./tests/actions/ -p
```

## actions.run

Runs action

### Arguments

* `ctx` - Action context (map)
* `name` - Action name (string)
* `next` - Next action (map or string) (interface)

### Returns

* `*` - Action results (interface)

## cmd.run

Runs command

### Arguments

* `args` - Command arguments (string or list) (interface)
* `name` - Command name (string)

### Returns

* `output` - Command output (interface)
* `success` - Execution state (bool)
* `time` - Execution time (user & system) (map)

## filter.trim

Trim spaces from strings

### Arguments

* `trim` - Name, a list or a map of values to trim (interface)

### Returns

* `*` - Action results (interface)

## html.extract

Extracts selectors from HTML body

### Arguments

* `body` - HTML body (string or byte array) (interface)
* `selectors` - Selectors to extract from HTML body (map)

### Returns

* `*` - Extracted selectors (interface)

## http.request

Makes a HTTP request

### Arguments

* `header` - Request header (map)
* `hostname` - Request hostname (string)
* `method` - Request method (string) (default: GET)
* `query` - Request query (map or string) (interface)
* `scheme` - Request URL scheme (http or https) (string) (default: http)
* `url` - Request URL (string)

### Returns

* `body` - Response body (bytes)
* `header` - Response header (map)
* `status` - Response status code (int)

## json.decode

Decode JSON values

### Arguments

* `decode` - Name, a list or a map of values to decode (interface)

### Returns

* `*` - Decoded values (interface)

## json.encode

Encode JSON values

### Arguments

* `encode` - Name, a list or a map of values to encode (interface)

### Returns

* `*` - Encoded values (interface)

## log.write

Writes a log

### Arguments

* `body` - Content to write (string)


## regexp.extract

Extracts regexp from text

### Arguments

* `regexp` - Regexp selectors (map)

### Returns

* `*` - Extracted text (interface)
