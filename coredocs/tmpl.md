# Core documentation
{{range $index, $fnc := .}}
## {{$index}}

{{$fnc.Description}}
{{if $fnc.Arguments}}
### Arguments
{{range $index, $var := $fnc.Arguments}}
* `{{$index}}` - {{if .Description}}{{.Description}}{{end}} ({{.Type}}){{if .Value}} (default: {{.Value}}){{end}}{{end}}{{end}}
{{if $fnc.Returns}}
### Returns
{{range $index, $var := $fnc.Returns}}
* `{{$index}}` - {{if .Description}}{{.Description}}{{end}} ({{.Type}}){{if .Value}} (default: {{.Value}}){{end}}{{end}}{{end}}
{{end}}