package main

import "os"
import "log"
import "flag"
import "text/template"
import "github.com/crackcomm/go-actions/core"

// Core functions
import _ "github.com/crackcomm/go-core"

var (
	docTemplate  *template.Template
	templateFile string
	outputFile   string
)

func main() {
	flag.Parse()
	// Parse template if not default
	var err error
	if templateFile != "" {
		docTemplate, err = template.ParseFiles(templateFile)
	} else {
		docTemplate, err = template.New("").Parse(defaultTemplate)
	}
	if err != nil {
		log.Fatal(err)
	}

	// Create output file or open and truncate
	output, err := os.OpenFile(outputFile, os.O_CREATE+os.O_TRUNC, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	defer output.Close()

	// Generate documentation and write to file
	err = docTemplate.Execute(output, core.Default.Functions)
	if err != nil {
		log.Fatal(err)
	}

	// Success
	log.Printf("Documentation saved to %s", outputFile)
}

var defaultTemplate = `# Core documentation
{{range $index, $fnc := .}}
## {{$index}}

{{$fnc.Description}}
{{if $fnc.Arguments}}
### Arguments
{{range $index, $var := $fnc.Arguments}}
* ` + "`" + `{{$index}}` + "`" + ` - {{if .Description}}{{.Description}}{{end}} ({{.Type}}){{if .Value}} (default: {{.Value}}){{end}}{{end}}{{end}}
{{if $fnc.Returns}}
### Returns
{{range $index, $var := $fnc.Returns}}
* ` + "`" + `{{$index}}` + "`" + ` - {{if .Description}}{{.Description}}{{end}} ({{.Type}}){{if .Value}} (default: {{.Value}}){{end}}{{end}}{{end}}
{{end}}`

func init() {
	flag.StringVar(&outputFile, "output", "docs.md", "Generated documentation save path.")
	flag.StringVar(&templateFile, "template", "", "Documentation template file.")
}
