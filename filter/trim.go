package filtercore

import "strings"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// Trim - filter.trim - trims all string values in context from `trim` list.
// Example:
// 	{
//		"name": "filter.trim",
//		"ctx": {
//			"trim": ["content", "other"],
//			"content": "   some content to trim out of white space    ",
//			"other": "   some other content to trim out of white space    "
//		}
//	}
func Trim(ctx action.Map) (action.Map, error) {
	// Pop trim value - context keys to trim
	ctx.Transform(ctx.Pop("trim"), trimFunc)

	return ctx, nil
}

// trimFunc - Runs strings.TrimSpace on strings, lists and maps.
func trimFunc(in action.Format) interface{} {
	// If it's a string just trim
	if value, ok := in.String(); ok {
		return strings.TrimSpace(value)
	}

	// If it's a list - deep trim all values
	if value, ok := in.List(); ok {
		for num, val := range value {
			if result := trimFunc(action.Format{val}); result != nil {
				value[num] = result
			}
		}
		return value
	}

	// If it's a map - deep trim all values
	if value, ok := in.Map(); ok {
		value.Transform(value.Keys(), trimFunc)
		return value
	}

	// Not expected type - don't touch
	return in.Value
}

func init() {
	core.Add("filter.trim", core.Function{
		Description: "Trim spaces from strings",
		Arguments: core.Variables{
			"trim": core.Interface("Name, a list or a map of values to trim", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Action results", nil),
		},
		Function: Trim,
	})
}
