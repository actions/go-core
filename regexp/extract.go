package regexpcore

import "fmt"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"

// Extract - Extracts patterns from context.
func Extract(ctx action.Map) (action.Map, error) {
	// Pop regexp value containig RegexpMap
	value := ctx.Pop("regexp")

	// If value is nil return error
	if value.IsNil() {
		return nil, fmt.Errorf("Regexp value can not be empty.")
	}

	// Convert value to a map
	remap, ok := value.Map()
	if !ok {
		return nil, fmt.Errorf("Regexp value `%#v` is not valid.", ctx.Get("regexp").Value)
	}

	// Convert a map to RegexpMap
	re := mapToRegexpMap(remap)

	// Extract all Regexps
	result := re.Extract(ctx)

	// Merge result into context
	ctx.Merge(result)

	// Return context
	return ctx, nil
}

func init() {
	core.Add("regexp.extract", core.Function{
		Description: "Extracts regexp from text",
		Arguments: core.Variables{
			"regexp": core.Map("Regexp selectors", nil),
		},
		Returns: core.Variables{
			"*": core.Interface("Extracted text", nil),
		},
		Function: Extract,
	})
}
