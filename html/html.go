package htmlcore

import "errors"
import "github.com/golang/glog"
import "github.com/crackcomm/go-actions/core"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-core/html/selector"

// ErrNoResults - Error returned when no results was found.
var ErrNoResults = errors.New("No results")

// ErrNoSelectors - Error returned when no selectors was found.
var ErrNoSelectors = errors.New("No selectors")

// Extract - extracts pieces of html in `body` using `selectors`
func Extract(ctx action.Map) (action.Map, error) {
	body, ok := ctx.Pop("body").Reader()
	if !ok {
		return ctx, selector.ErrNoBody
	}
	defer body.Close()

	selectors, ok := ctx.Pop("selectors").Map()
	if !ok {
		return ctx, ErrNoSelectors
	}

	glog.V(2).Infof("html.extract selectors=%v\n", selectors)

	// Extract from body
	result, err := ExtractSelectors(selectors, body)
	if err != nil {
		glog.V(2).Infof("html.extract err=%v\n", err)
		return ctx, err
	}

	glog.V(2).Infof("html.extract result=%v\n", result)

	return result, nil
}

func init() {
	core.Add("html.extract", core.Function{
		Description: "Extracts selectors from HTML body",
		Arguments: core.Variables{
			"selectors": core.Map("Selectors to extract from HTML body", nil),
			"body":      core.Stream("HTML body"),
		},
		Returns: core.Variables{
			"*": core.Interface("Extracted selectors", nil),
		},
		Function: Extract,
	})
}
