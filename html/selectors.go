package htmlcore

import "io"
import "github.com/crackcomm/go-actions/action"
import "github.com/crackcomm/go-core/html/selector"

// ExtractSelectors - Extracts selectors from body.
func ExtractSelectors(selectors action.Map, body io.Reader) (result action.Map, err error) {
	// Check existence of body
	if body == nil {
		err = selector.ErrNoBody
		return
	}

	// Get map selectors
	sel := MapSelectors(selectors)

	// Extract selectors from body
	res, err := sel.ExtractReader(body)
	if err != nil {
		return
	}

	// Transform to map
	result, ok := res.(action.Map)
	if !ok {
		err = ErrNoResults
	}

	return
}

// MapSelectors - Iterates over map of selectors initializaing proper structures.
func MapSelectors(selectors action.Map) (res selector.SelectorsMap) {
	res = selector.SelectorsMap{}

	// Map all values in selectors map
	for name := range selectors {
		// Get value by name
		value := selectors.Get(name)

		// If value is a selector create structures and continue
		if IsSelector(value) {
			res[name] = ToSelector(value)
			continue
		}

		// If it's not a selector but a map it's a map of selectors
		if m, ok := value.Map(); ok {
			res[name] = MapSelectors(m)
		}
	}

	// Return result
	return
}

// IsSelector - Checks if value is a selector.
func IsSelector(value action.Format) bool {
	// If value is a string it's a selector
	if _, ok := value.String(); ok {
		return true
	}

	// Maybe it's a map ?
	m, ok := value.Map()
	if !ok {
		return false
	}

	// If we formatted this map save it
	value.Value = m

	// If map contains $path it's a selector
	if !m.Get("$path").IsNil() {
		return true
	}

	// If map contains $extract it's a selector
	if !m.Get("$extract").IsNil() {
		return true
	}

	return false
}

// ToSelector - Transforms a value into a Selector.
// Accepts string and map values.
func ToSelector(value action.Format) selector.Extractor {
	// Try to get string value
	if path, ok := value.String(); ok {
		return &selector.Selector{
			Extractor: selector.TextExtractor,
			Path:      path,
		}
	}

	// Maybe it's a map...
	if m, ok := value.Map(); ok {
		return mapToSelector(m)
	}

	return nil
}

// mapToSelector - Creates an Extractor ouf of action.Map.
func mapToSelector(m action.Map) selector.Extractor {
	// Selector path
	path, _ := m.Get("$path").String()
	// Selector extractor (it can be a string (attribute extractor), empty (text extractor))
	// it can be another selector or map of selectors
	extract := m.Get("$extract")

	// If $extract is empty - extractor is TextExtractor
	if extract.IsNil() {
		return &selector.Selector{
			Extractor: selector.TextExtractor,
			Path:      path,
		}
	}

	// If $extract is a string, extractor is an attribute Extractor
	if name, ok := extract.String(); ok {
		return &selector.Selector{
			Path:      path,
			Extractor: selector.GetExtractor(name),
		}
	}

	// If $extract is a selector we have to create deep selector
	if IsSelector(extract) {
		return &selector.Selector{
			Path:      path,
			Extractor: ToSelector(extract),
		}
	}

	// Or a map of selectors
	if m, ok := extract.Map(); ok {
		return &selector.Selector{
			Path:      path,
			Extractor: MapSelectors(m),
		}
	}

	// $extract is not expected value
	return nil
}
