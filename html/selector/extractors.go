package selector

import "regexp"
import "strings"
import "code.google.com/p/go.net/html"

// textExtractor - Extracts text from a node. Implements Extractor interface.
type textExtractor int

// numberExtractor - Extracts number from node's content. Implements Extractor interface.
type numberExtractor int

// AttributeExtractor - Extracts an attribute from a node. Implements Extractor interface.
type AttributeExtractor struct {
	Attribute string
}

// TextExtractor - So we don't have to allocate a new one everytime.
var TextExtractor = new(textExtractor)

// NumberExtractor - So we don't have to allocate a new one everytime.
var NumberExtractor = new(numberExtractor)

// GetExtractor - Returns extractor by name. If none was found returns AttributeExtractor extracting name as attribute.
func GetExtractor(name string) Extractor {
	switch name {
	case "text":
		return TextExtractor
	case "number":
		return NumberExtractor
	}
	return &AttributeExtractor{name}
}

// Extract - Extracts node text.
func (e textExtractor) Extract(node *html.Node) interface{} {
	// Return conten if it's text
	if node.Type == html.TextNode {
		return strings.TrimSpace(node.Data)
	}

	var text string
	// Get text from all childs
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		if more, ok := e.Extract(child).(string); ok {
			text += more
		}
	}

	// Return result
	return strings.TrimSpace(text)
}

// numre - finds numbers in string
var numre = regexp.MustCompile("([0-9]+)")

// Extract - Extracts number from node's content.
func (e numberExtractor) Extract(node *html.Node) interface{} {
	// Extract text, then all numbers from it and join to one string
	if text, ok := TextExtractor.Extract(node).(string); ok && len(text) > 0 {
		return strings.Join(numre.FindAllString(text, -1), "")
	}

	// Text was not found
	return nil
}

// Extract - Extracts node Attribute.
func (e *AttributeExtractor) Extract(node *html.Node) interface{} {
	// Look for `e.Attibute` attribute
	for _, a := range node.Attr {
		if a.Key == e.Attribute {
			return a.Val
		}
	}

	// Attribute was not found
	return nil
}
