package selector

import "io"
import "bytes"
import "errors"
import "code.google.com/p/go.net/html"
import "code.google.com/p/go-html-transform/h5"
import "github.com/crackcomm/go-actions/action"

// ErrNoBody - Error returned when no body was found.
var ErrNoBody = errors.New("No body")

// SelectorsMap - Map of selectors.
type SelectorsMap map[string]Extractor

// Extract - Extracts all selectors and returns result.
func (m SelectorsMap) Extract(node *html.Node) interface{} {
	result := action.Map{}

	// Iterate over all selectors in the map
	for name, selector := range m {
		if selector == nil {
			continue
		}

		// Extract selector and save anything was found
		if value := selector.Extract(node); value != nil {
			result[name] = value
		}
	}

	// Return nil if empty result so it can be discarded
	if len(result) == 0 {
		return nil
	}

	return result
}

// ExtractBytes - Acts like Extract but first parses html body.
func (m SelectorsMap) ExtractBytes(body []byte) (res interface{}, err error) {
	return m.ExtractReader(bytes.NewBuffer(body))
}

// ExtractReader - Acts like Extract but first parses html body from reader.
func (m SelectorsMap) ExtractReader(buffer io.Reader) (res interface{}, err error) {
	// If no buffer return error
	if buffer == nil {
		err = ErrNoBody
		return
	}

	// Parse body
	var node *h5.Tree
	node, err = h5.New(buffer)
	if err != nil {
		return
	}

	// Extract from top node
	res = m.Extract(node.Top())
	return
}
