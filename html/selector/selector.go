package selector

import "code.google.com/p/cascadia"
import "code.google.com/p/go.net/html"

// Extractor - selects proper values from html node
type Extractor interface {
	Extract(*html.Node) interface{}
}

// Selector - structure that selects  (implements Extractor interface)
type Selector struct {
	Path      string
	Extractor Extractor
}

// Extract - Selects html node by Path and extracts using Extractor.
func (s *Selector) Extract(node *html.Node) interface{} {
	// We can't do magic (hehe...)
	if node == nil {
		return nil
	}

	// If no Extractor was set we will extract text from this node :)
	if s.Extractor == nil {
		s.Extractor = TextExtractor
	}

	// If path is empty extract on current node
	if s.Path == "" {
		return s.Extractor.Extract(node)
	}

	// Compile Path to real selector
	selector, err := cascadia.Compile(s.Path)
	if err != nil {
		return nil
	}

	// Select all nodes
	nodes := selector.MatchAll(node)

	// If no nodes was found return nil
	if len(nodes) == 0 {
		return nil
	}

	// If found only one node extract first
	if len(nodes) == 1 {
		first := nodes[0]
		return s.Extractor.Extract(first)
	}

	// Extract from all found nodes
	result := make([]interface{}, len(nodes))
	for num, n := range nodes {
		result[num] = s.Extractor.Extract(n)
	}

	return result
}
