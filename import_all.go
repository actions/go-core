// Package gocore imports all core packages.
package gocore

import (
	_ "github.com/crackcomm/go-core/actions"
	_ "github.com/crackcomm/go-core/cmd"
	_ "github.com/crackcomm/go-core/context"
	_ "github.com/crackcomm/go-core/filter"
	_ "github.com/crackcomm/go-core/html"
	_ "github.com/crackcomm/go-core/http"
	_ "github.com/crackcomm/go-core/json"
	_ "github.com/crackcomm/go-core/log"
	_ "github.com/crackcomm/go-core/regexp"
)
